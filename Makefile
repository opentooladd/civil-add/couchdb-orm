.PHONY: coverage

.env:
	cp .env.tpl .env

include .env

start-couchdb:
	docker-compose up --build couchdb

start-dev:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up rust

start:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up 

watch: ~/.cargo/bin/watchexec
	watchexec --exts rs,tpl make start-dev

build-couchdb: build-image

build: Dockerfile
	DOCKER_BUILDKIT=1 docker build . --progress=plain

create-base-databases:
	curl -X PUT "http://localhost:5984/_users" -u $(COUCHDB_ORM_NAME):$(COUCHDB_ORM_PASSWORD)
	curl -X PUT "http://localhost:5984/_replicator" -u $(COUCHDB_ORM_NAME):$(COUCHDB_ORM_PASSWORD)
	curl -X PUT "http://localhost:5984/_global_changes" -u $(COUCHDB_ORM_NAME):$(COUCHDB_ORM_PASSWORD)

bust-cache:
	docker builder prune --filter type=exec.cachemount

test:
	cargo test

coverage: ~/.cargo/bin/cargo-tarpaulin
	cargo tarpaulin --output-dir coverage --exclude-files __tests__/* --exclude-files _meta/*

install:
	cargo install --path .

build-base-image: infra/couchdb-docker/3.2.2
	@echo building base Docker Image for COUCHDB-DOCKER
	@echo BASE_COUCHDB_IMAGE_NAME = $(BASE_COUCHDB_IMAGE_NAME)
	@echo COUCHDB_VERSION = $(COUCHDB_VERSION)
	docker build -t $(BASE_COUCHDB_IMAGE_NAME) $(COUCHDB_DOCKER_FOLDER)$(COUCHDB_VERSION)

build-image: build-base-image couchdb/Dockerfile
	@echo building Docker Image for COUCHDB
	@echo COUCHDB_CONTAINER_NAME = $(COUCHDB_CONTAINER_NAME)
	@echo COUCHDB_VERSION = $(COUCHDB_VERSION)
	docker build -t $(COUCHDB_CONTAINER_NAME) couchdb

couchdb/Dockerfile: .env
	@echo "generating couchdb/Dockerfile"
	rm -f couchdb/Dockerfile
	touch couchdb/Dockerfile
	@echo "FROM $(BASE_COUCHDB_IMAGE_NAME)" >> couchdb/Dockerfile
	@echo "COPY local.ini /opt/couchdb/etc" >> couchdb/Dockerfile

infra/couchdb-docker/3.2.2:
	git submodule update --recursive --init

~/.cargo/bin/cargo-tarpaulin:
	cargo install cargo-tarpaulin

~/.cargo/bin/watchexec:
	cargo install watchexec
