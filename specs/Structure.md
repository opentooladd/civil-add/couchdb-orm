# Structure

The goal of this structure is to ensure consistency with various database actions manipulating models and schema types.

## Assumptions

- Each **database** has only **1 schema** associated that can evolve
    - Each evolution **MUST** result in a register operation
- A **migration** is performed between 2 schemas from **different versions**
    - A **migration** needs 2 schemas.
    - A **migration** can be performed in the 2 ways (old -> new, new -> old)

## Folder structure

**Root folder**: `_meta`

```
/_meta
    |- migrations/
        |- {db_name}
            |- {timestamp}.rs
            mod.rs
        mod.rs
    mod.rs
    |- seeds/
        |- {db_name}
            |- {timestamp}.rs
            mod.rs
        mod.rs
    mod.rs
    |- schemas/
        |- {db_name}
            |- from_schema_{timestamp}_to_schema_{timestamp}.rs
            mod.rs
        mod.rs
    mod.rs
mod.rs
```
