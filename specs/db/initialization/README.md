# DB Initialization specs

## Infra Prerequisite

- **DB Server** should be available on a **HOST**
    - :heavy_check_mark: If exists -> continue
    - :red_circle: If don't exist -> abort with error
- **DB table** should not exists
    - :red_circle: If dont'exists -> create database
    - :heavy_check_mark: If exists -> abort with error

