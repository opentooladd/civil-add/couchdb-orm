# Migrations

Migrations will be based on the [From trait](https://doc.rust-lang.org/std/convert/trait.From.html).
It should be implemented on every schema that needs a transition.
It should be implemented for the *reference* of the source 

## Procedure

Migration operation will be operated through these couchdb URI:
- POST /{db}/_all_docs?include_docs=true
    - This is to get the documents data for the migrations.
    The schema <T> + the `_id` and `_rev` for the bulk update.
    @TODO pagination and limits
    @TODO check schema on database to see if it corresponds to the src schema of the migration
- POST /{db}/_bulk_docs
    - This route is for the update of the docs.
    couchdb accept the PUT as a full replacement of the document.

## Troubleshooting

For the moment, all the needed struct are to be in the same file.
It is 1 file for 1 db.

@TODO
- Handle external types dependencies:
    - single deps
    - common deps
