# DB seeds spec

## Infra Prerequisite

- **DB Server** should be available on a **HOST**
    - :heavy_check_mark: If exists -> continue
    - :red_circle: If don't exist -> abort with error
- **DB table** should exist
    - :red_circle: If exists -> abort with error
    - :heavy_check_mark: If dont'exists -> create database
- **DB table** should be empty
    - :heavy_check_mark: If empty -> seed database
    - :red_circle: If not empty -> ask if it should empty
        - :heavy_check_mark: If yes -> empty and seed
        - :red_circle: If not empty -> abort

## Functionnal prerequisite

- **Schema** to use the corresponding data type
    - :heavy_check_mark: If exists -> continue
    - :red_circle: If don't exist -> abort with error

## How to use

The CLI command will create a seeding file for the corresponding database including the last schema registered for this document.

```
use my_schema::MyModel;

pub fn seed() -> Vec<MyModel> {
    unimplemented!()
}
```

The method get seed will be used to seed the table.
