// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use std::fs;
use std::path::PathBuf;
use std::str::FromStr;

#[allow(dead_code)]
pub const TESTS_FOLDER: &'static str = "__tests__";

#[allow(dead_code)]
pub fn root_test_path() -> PathBuf {
    std::env::current_dir()
        .unwrap()
        .join(PathBuf::from_str(TESTS_FOLDER).unwrap())
}

#[allow(dead_code)]
pub fn clean_meta_dir(rootdir: &PathBuf) {
    let meta_path = rootdir.join(PathBuf::from("_meta"));
    if meta_path.exists() {
        fs::remove_dir_all(&meta_path).expect("failed to clean test _meta dir")
    }
}

#[allow(dead_code)]
pub fn clean_test_dir(rootdir: &PathBuf) {
    if rootdir.exists() {
        fs::remove_dir_all(&rootdir).expect("failed to clean test dir")
    }
}

#[allow(dead_code)]
pub fn create_tests_folder() {
    fs::create_dir_all(TESTS_FOLDER).unwrap()
}

#[allow(dead_code)]
pub fn create_test_dir(path: &str) -> PathBuf {
    let p = root_test_path().join(path);
    if p.exists() {
        fs::remove_dir_all(&p).expect("failed destroy test dir")
    }
    fs::create_dir_all(&p).unwrap();
    p
}
