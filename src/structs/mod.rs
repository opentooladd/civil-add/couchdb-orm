// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use std::collections::HashMap;
use uuid::Uuid;

#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct DesignDoc {
  #[serde(rename = "_id")]
  pub id: Option<String>,
  #[serde(rename = "_rev", skip_serializing_if = "Option::is_none")]
  pub rev: Option<String>,
  pub views: HashMap<String, HashMap<String, String>>,
}

impl DesignDoc {
  pub fn new(views: HashMap<String, HashMap<String, String>>) -> DesignDoc {
    DesignDoc {
      id: Some(Uuid::new_v4().to_string()),
      rev: None,
      views,
    }
  }
}