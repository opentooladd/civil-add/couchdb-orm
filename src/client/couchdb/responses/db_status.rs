// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Size {
    pub file: usize,
    pub external: usize,
    pub active: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DbStatus {
    pub db_name: String,
    pub purge_seq: String,
    pub update_seq: String,
    pub sizes: Size,
    pub props: serde_json::Value,
    pub doc_del_count: usize,
    pub doc_count: usize,
    pub disk_format_version: usize,
    pub compact_running: bool,
    pub cluster: HashMap<String, usize>,
    pub instance_start_time: String,
}
