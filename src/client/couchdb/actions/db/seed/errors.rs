// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Error, Serialize, Deserialize, Clone)]
#[error("DBSeedError: {0}")]
pub struct DBSeedError(String);

impl DBSeedError {
    pub fn new(message: &str) -> Self {
        DBSeedError(format!("{}", message))
    }
}

#[derive(Debug, Error, Serialize, Deserialize, Clone)]
#[error("DBSeedNotEmpty: {0}")]
pub struct DBSeedNotEmpty(String);

impl DBSeedNotEmpty {
    pub fn new(db_name: &str) -> Self {
        DBSeedNotEmpty(format!("Database {} not empty", db_name))
    }
}
