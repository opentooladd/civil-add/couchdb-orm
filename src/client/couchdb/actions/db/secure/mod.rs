// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use crate::client::couchdb::models::Security;
use crate::regexes::COUCHDB_DB_RULE;
use awc::Client;

pub mod errors;

use errors::DBSecureError;

pub async fn secure_db(
    client: &Client,
    db_name: &str,
    data: Security,
    host: &str,
) -> Result<bool, Box<dyn std::error::Error>> {
    if !COUCHDB_DB_RULE.is_match(db_name) {
        return Err(Box::new(DBSecureError::new(&format!(
            "{} string doesn't respect the regex rule {}",
            db_name,
            COUCHDB_DB_RULE.as_str()
        ))));
    }

    // println!("{:?}", db_status);
    let uri: String = format!("{}/{}/_security", host, db_name,);
    // println!("{:?}", serde_json::to_string(&data).unwrap());
    match client.put(&uri).send_json(&data).await {
        Ok(mut response) => {
            // println!("{:?}", response);
            if response.status().as_u16() >= 400 {
                let bytes: Vec<u8> = response.body().await?.iter().cloned().collect();
                let error: serde_json::Value =
                    serde_json::from_str(std::str::from_utf8(bytes.as_slice()).unwrap()).unwrap();
                Err(Box::new(DBSecureError::new(&format!(
                    "Error with the request to {}: error: \n{}",
                    &uri, error
                ))))
            } else {
                Ok(true)
            }
        }
        Err(error) => Err(Box::new(DBSecureError::new(&format!(
            "Error with the request to {}: error: \n{}",
            host, error
        )))),
    }
}

#[cfg(test)]
mod tests {}
