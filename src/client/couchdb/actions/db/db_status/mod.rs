// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use crate::regexes::COUCHDB_DB_RULE;
use awc::{Client, ClientRequest};

pub mod errors;

use crate::client::couchdb::responses::db_status::DbStatus;
use errors::DBStatusError;

pub async fn get_db_status(
    client: &Client,
    db_name: &str,
    host: &str,
) -> Result<DbStatus, Box<dyn std::error::Error>> {
    if !COUCHDB_DB_RULE.is_match(db_name) {
        return Err(Box::new(DBStatusError::new(&format!(
            "{} string doesn't respect the regex rule {}",
            db_name,
            COUCHDB_DB_RULE.as_str()
        ))));
    }
    let uri: String = format!("{}/{}", host, db_name,);
    let request: ClientRequest = client.get(&uri);
    // println!("{:?}", request);
    match request.send().await {
        Ok(mut response) => {
            // println!("{}", &uri);
            // println!("{:?}", response);
            if response.status().as_u16() >= 400 {
                let bytes: Vec<u8> = response.body().await?.iter().cloned().collect();
                let error: serde_json::Value =
                    serde_json::from_str(std::str::from_utf8(bytes.as_slice()).unwrap()).unwrap();
                Err(Box::new(DBStatusError::new(&format!(
                    "Error with the request to {}: error: \n{}",
                    &uri, error
                ))))
            } else {
                let bytes: Vec<u8> = response.body().await?.iter().cloned().collect();
                let value: DbStatus = serde_json::from_str({
                    let res = std::str::from_utf8(bytes.as_slice()).unwrap();
                    res
                })
                .unwrap();
                Ok(value)
            }
        }
        Err(error) => Err(Box::new(DBStatusError::new(&format!(
            "Error with the request to {}: error: \n{}",
            host, error
        )))),
    }
}
