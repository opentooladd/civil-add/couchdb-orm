// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use crate::futures::Future;
use awc::Client;
use serde::{de::DeserializeOwned, Serialize};

// useless for the moment
pub trait Migration<S, D>
where
    S: Clone + Serialize + DeserializeOwned,
    D: Clone + From<S> + Serialize + DeserializeOwned,
{
    fn execute_migration(
        client: &'static Client,
        db_name: String,
        host: String,
    ) -> Box<dyn Future<Output = Result<bool, Box<dyn std::error::Error>>>>;
}
