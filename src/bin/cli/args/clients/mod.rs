// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use std::str::FromStr;

pub mod errors;

use errors::ClientsParsingError;

#[derive(Debug)]
pub enum Clients {
    CouchDB,
}

impl Clients {
    pub fn to_list() -> [&'static str; 1] {
        ["couchdb"]
    }
}

impl FromStr for Clients {
    type Err = ClientsParsingError;

    fn from_str(value: &str) -> Result<Clients, Self::Err> {
        if value.to_lowercase() == "couchdb" {
            Ok(Clients::CouchDB)
        } else {
            Err(ClientsParsingError::new(format!(
                "{} is not a valid client, try one of {:?}",
                value,
                Clients::to_list()
            )))
        }
    }
}

impl From<&str> for Clients {
    fn from(value: &str) -> Clients {
        Clients::from_str(value).unwrap()
    }
}

impl From<String> for Clients {
    fn from(value: String) -> Clients {
        Clients::from(value.as_str())
    }
}
