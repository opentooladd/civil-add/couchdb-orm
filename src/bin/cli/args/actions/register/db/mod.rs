// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use std::fs;
use std::path::PathBuf;
use std::str::FromStr;

use couchdb_orm::utils::{append_to_file, init_meta_dir};

pub fn register_db(name: &str, rootdir: &PathBuf) -> Result<(), Box<dyn std::error::Error>> {
    init_meta_dir(&rootdir)?;
    let schemas_db_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/schemas/{}",
        rootdir.to_str().unwrap(),
        name
    ))
    .unwrap();
    let migrations_db_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/migrations/{}",
        rootdir.to_str().unwrap(),
        name
    ))
    .unwrap();
    let seeds_db_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/seeds/{}",
        rootdir.to_str().unwrap(),
        name
    ))
    .unwrap();
    let securities_db_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/securities/{}",
        rootdir.to_str().unwrap(),
        name
    ))
    .unwrap();
    let design_docs_db_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/design_docs/{}",
        rootdir.to_str().unwrap(),
        name
    ))
    .unwrap();

    if !schemas_db_path.exists() {
        fs::create_dir_all(&schemas_db_path)?;
        append_to_file(
            &PathBuf::from_str(&format!(
                "{}/_meta/src/schemas/mod.rs",
                rootdir.to_str().unwrap()
            ))
            .unwrap(),
            &format!("pub mod {};", &name),
        )?;
    }
    if !migrations_db_path.exists() {
        fs::create_dir_all(&migrations_db_path)?;
        append_to_file(
            &PathBuf::from_str(&format!(
                "{}/_meta/src/migrations/mod.rs",
                rootdir.to_str().unwrap()
            ))
            .unwrap(),
            &format!("pub mod {};", &name),
        )?;
    }
    if !seeds_db_path.exists() {
        fs::create_dir_all(seeds_db_path)?;
        append_to_file(
            &PathBuf::from_str(&format!(
                "{}/_meta/src/seeds/mod.rs",
                rootdir.to_str().unwrap()
            ))
            .unwrap(),
            &format!("pub mod {};", &name),
        )?;
    }
    if !securities_db_path.exists() {
        fs::create_dir_all(securities_db_path)?;
        append_to_file(
            &PathBuf::from_str(&format!(
                "{}/_meta/src/securities/mod.rs",
                rootdir.to_str().unwrap()
            ))
            .unwrap(),
            &format!("pub mod {};", &name),
        )?;
    }
    if !design_docs_db_path.exists() {
        fs::create_dir_all(design_docs_db_path)?;
        append_to_file(
            &PathBuf::from_str(&format!(
                "{}/_meta/src/design_docs/mod.rs",
                rootdir.to_str().unwrap()
            ))
            .unwrap(),
            &format!("pub mod {};", &name),
        )?;
    }

    let schemas_mod_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/schemas/{}/mod.rs",
        rootdir.to_str().unwrap(),
        name
    ))
    .unwrap();
    if !schemas_mod_path.exists() {
        fs::write(
            &schemas_mod_path,
            include_str!("../../../../../../../static/templates/schemas_mod_file.tpl"),
        )?;
    }
    let migration_mod_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/migrations/{}/mod.rs",
        rootdir.to_str().unwrap(),
        name
    ))
    .unwrap();
    if !migration_mod_path.exists() {
        fs::write(
            &migration_mod_path,
            include_str!("../../../../../../../static/templates/migrations_mod_file.tpl"),
        )?;
    }
    let seeds_mod_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/seeds/{}/mod.rs",
        rootdir.to_str().unwrap(),
        name
    ))
    .unwrap();
    if !seeds_mod_path.exists() {
        fs::write(
            &seeds_mod_path,
            include_str!("../../../../../../../static/templates/seeds_mod_file.tpl"),
        )?;
    }
    let securities_mod_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/securities/{}/mod.rs",
        rootdir.to_str().unwrap(),
        name
    ))
    .unwrap();
    if !securities_mod_path.exists() {
        fs::write(
            &securities_mod_path,
            include_str!("../../../../../../../static/templates/securities_mod_file.tpl"),
        )?;
    }
    Ok(())
}
