// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use crate::cli::args::actions::register::db::register_db;
use couchdb_orm::utils::{append_to_file, init_meta_dir, main_content};
use serde::{Deserialize, Serialize};
use std::fs;
use std::path::PathBuf;
use std::str::FromStr;
use thiserror::Error;

#[derive(Debug, Error, Serialize, Deserialize, Clone)]
#[error("RegisterSecurityError: {0}")]
struct RegisterSecurityError(String);

fn write_main_file(rootdir: &PathBuf) -> Result<(), Box<dyn std::error::Error>> {
    let main_file_name: PathBuf =
        PathBuf::from_str(&format!("{}/_meta/src/main.rs", rootdir.to_str().unwrap())).unwrap();

    let main_content: String = main_content(rootdir)?;

    // println!("{:#?}", main_content);
    fs::write(main_file_name, main_content)?;

    Ok(())
}

fn write_security_db_mod(
    db_path: &PathBuf,
    filename: &PathBuf,
    rootdir: &PathBuf,
) -> Result<(), Box<dyn std::error::Error>> {
    // println!("{}", db_path);
    let securities_path: PathBuf = db_path.clone();
    let mod_file_name: PathBuf =
        PathBuf::from_str(&format!("{}/mod.rs", db_path.display())).unwrap();

    let available_securitys: Vec<PathBuf> = fs::read_dir(&securities_path)?
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<PathBuf>, std::io::Error>>()?;
    let available_securitys: Vec<&PathBuf> = available_securitys
        .iter()
        .filter(|e| e.file_name().unwrap() != "mod.rs")
        .collect();

    let available_securitys_mod_filenames: String = available_securitys
        .iter()
        .map(|e| {
            let mut s = e
                .file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .to_string()
                .replace(".rs", ";");
            s.push_str("\n");
            format!("pub mod {}", s)
        })
        .collect();
    let mut available_securitys_enum: Vec<String> = available_securitys
        .iter()
        .map(|e| {
            e.file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .to_string()
                .replace(".rs", "")
        })
        .collect();
    let filename_str: String = format!(
        r#"{}"#,
        filename
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .to_string()
            .replace(".rs", "")
    );
    if !available_securitys_enum.contains(&filename_str) {
        available_securitys_enum.push(filename_str.clone())
    }
    let available_securitys_enum: String = available_securitys_enum.join(",\n");
    let mut available_securitys_list: Vec<String> = available_securitys
        .iter()
        .map(|e| {
            let s = e
                .file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .to_string()
                .replace(".rs", "");
            let s = format!(r#""{}""#, s);
            s
        })
        .collect();
    let filename_str: String = format!(r#""{}""#, &filename_str);

    if !available_securitys_list.contains(&filename_str) {
        available_securitys_list.push(filename_str.clone());
    }
    let available_securitys_list: String = available_securitys_list.join(",\n");
    let available_security_content: String = format!(
        include_str!("../../../../../../../static/templates/available_securities.tpl"),
        available_securitys_mod_filenames, available_securitys_enum, available_securitys_list
    );
    // println!("{:?}", available_security_content);
    fs::write(mod_file_name, available_security_content)?;

    write_main_file(rootdir)?;
    Ok(())
}

fn write_security_file(
    db_path: &PathBuf,
    db_name: &str,
    rootdir: &PathBuf,
) -> Result<PathBuf, Box<dyn std::error::Error>> {
    let file_name: PathBuf =
        PathBuf::from_str(&format!("{}/security_{}.rs", db_path.display(), db_name))?;
    let file_content: String = format!(
        include_str!("../../../../../../../static/templates/security_file.tpl"),
        db_name
    );

    if !file_name.exists() {
        fs::write(&file_name, &file_content)?;
    }

    write_security_db_mod(db_path, &file_name, rootdir)?;
    Ok(file_name)
}

pub fn register_security(
    db_name: String,
    rootdir: &PathBuf,
) -> Result<(), Box<dyn std::error::Error>> {
    init_meta_dir(&rootdir)?;
    let db_path: PathBuf = PathBuf::from_str(&format!(
        "{}/_meta/src/securities/{}",
        rootdir.to_str().unwrap(),
        db_name
    ))?;

    register_db(&db_name, rootdir)?;
    let security_file = write_security_file(&db_path, &db_name, &rootdir)?;
    println!(
        "you can edit you security file here: {}",
        security_file.display()
    );
    Ok(())
}
