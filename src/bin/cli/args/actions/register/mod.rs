// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use structopt::StructOpt;

use std::path::PathBuf;

pub mod db;
pub mod migration;
pub mod schema;
pub mod security;
pub mod seed;
pub mod design_doc;

#[derive(Debug, StructOpt)]
pub enum RegisterActions {
    /// Register a db
    /// This gives automatisation of initialization
    Db { name: String },
    /// Register a security object for a db
    Security {
        /// The database name for which to create a security
        db_name: String,
    },
    /// Register a schema a path {arg}
    /// The schema must be serializable, deserializable with serde
    Schema {
        /// The path to the schema to register
        path: PathBuf,
        /// The database name in which the schema belongs
        db_name: String,
        /// should it override the last version ?
        #[structopt(long = "override", short = "over")]
        override_version: bool,
    },
    /// Register a migration based on the registered schemas
    Migration {
        /// The database name in which the schema belongs
        db_name: String,
        /// should it override the last version ?
        #[structopt(long = "override", short = "over")]
        override_version: bool,
    },
    /// Register a seed based on the registered schemas
    Seed {
        /// The database name in which the schema belongs
        db_name: String,
        /// The name of the seed
        name: String,
        /// should it override the last version ?
        #[structopt(long = "override", short = "over")]
        override_version: bool,
    },
    /// Register a design doc for a given database
    DesignDoc {
        /// The database name for which the design doc is designed
        db_name: String,
        /// The name of the design doc
        name: String,
        /// should it override the last version ?
        #[structopt(long = "override", short = "over")]
        override_version: bool,
        /// Add a Design Doc Object
        #[structopt(subcommand)]
        add: Option<AddActions>
    },
}

#[derive(Debug, StructOpt)]
pub enum AddActions {
    View {
        /// The name of the view
        name: String,
    }
}
