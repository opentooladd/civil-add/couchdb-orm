// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub enum DBActions {
    /// Create a database with name
    Create { names: Vec<String> },
    /// Delete a database with name
    Delete { names: Vec<String> },
    /// Delete all docs from a database
    DeleteDocs { db_name: String },
    /// Get all documents
    AllDocs { db_name: String },
    /// Backups the document as a seed and a json file
    Backup { db_name: String },
    /// Restore the document based on JSON backups
    Restore { db_name: String },
    /// secure
    Secure {
        db_name: String,
        file: Option<String>,
    },
    /// migrate
    Migrate { db_name: String },
    /// seed
    Seed { db_name: String },
}
