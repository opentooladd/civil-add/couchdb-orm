// Copyright (C) 2020-2023  OpenToolAdd
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// contact: contact@tool-add.com

extern crate structopt;

use structopt::StructOpt;

use futures::stream::{self, StreamExt};
use futures::FutureExt;

mod cli;

use cli::args::actions::db::DBActions;
use cli::args::actions::design_doc::DesignDocActions;
use cli::args::actions::register::db::register_db;
use cli::args::actions::register::migration::register_migration;
use cli::args::actions::register::schema::register_schema;
use cli::args::actions::register::security::register_security;
use cli::args::actions::register::seed::register_seed;
use cli::args::actions::register::design_doc::register_design_doc;
use cli::args::actions::register::design_doc::register_design_doc_view;
use cli::args::actions::register::{ RegisterActions, AddActions };
use cli::args::actions::Actions;
use cli::args::clients::Clients;
use cli::args::CliArgs;

use couchdb_orm::client::couchdb::CouchDBClient;

const LICENSE: &'static str = include_str!("../../LICENSE");

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt: CliArgs = CliArgs::from_args();
    // println!("{:?}", opt);

    match opt.client {
        Clients::CouchDB => {
            let client: CouchDBClient = CouchDBClient::new(None);
            match opt.action {
                Actions::DB(action) => match action {
                    DBActions::Create { names } => {
                        println!("db create {:?}", names);
                        let futs: Vec<_> =
                            names.iter().map(|n| client.create_db(&n).fuse()).collect();
                        let mut stream = stream::iter(futs);

                        while let Some(item) = stream.next().await {
                            match item.await {
                                Ok(r) => println!("{:#?}", r),
                                Err(e) => println!("{}", e),
                            }
                        }
                    }
                    DBActions::Delete { names } => {
                        println!("db delete {:?}", names);
                        let futs: Vec<_> =
                            names.iter().map(|n| client.delete_db(&n).fuse()).collect();
                        let mut stream = stream::iter(futs);

                        while let Some(item) = stream.next().await {
                            match item.await {
                                Ok(r) => println!("{:#?}", r),
                                Err(e) => println!("{}", e),
                            }
                        }
                    }
                    DBActions::AllDocs { db_name } => {
                        println!("db {} get all docs", db_name);
                        client.get_all_docs(&db_name).await?;
                    }
                    DBActions::DeleteDocs { db_name } => {
                        println!("db {} delete all docs", db_name);
                        client.delete_all_docs(&db_name).await?;
                    }
                    DBActions::Backup { db_name } => {
                        println!("db backup {}", db_name);
                        client.backup_db(&db_name, &opt.rootdir).await?;
                    }
                    DBActions::Restore { db_name } => {
                        println!("db restore {}", db_name);
                        client.restore_db(&db_name, &opt.rootdir).await?;
                    }
                    DBActions::Secure { db_name, file } => {
                        println!("db secure");
                        client.secure_db(&db_name, file, &opt.rootdir).await?;
                    }
                    DBActions::Migrate { db_name } => {
                        println!("db migrate");
                        client.migrate_db(&db_name, &opt.rootdir).await?;
                    }
                    DBActions::Seed { db_name } => {
                        println!("db seed");
                        client.seed_db(&db_name, &opt.rootdir).await?;
                    },
                },
                Actions::DesignDoc(action) => match action {
                    DesignDocActions::Create { db_name } => {
                        println!("design doc create");
                        client.create_design_doc(&db_name, &opt.rootdir).await?;
                    }
                },
                Actions::Register(action) => match action {
                    RegisterActions::Db { name } => {
                        println!("registering db: name: {}", name);
                        register_db(&name, &opt.rootdir)?;
                    }
                    RegisterActions::Schema {
                        path,
                        db_name,
                        override_version,
                    } => {
                        println!("registering schema: path: {:?}, db_name: {}", path, db_name);
                        register_schema(path, db_name, override_version, &opt.rootdir)?;
                    }
                    RegisterActions::Migration {
                        db_name,
                        override_version,
                    } => {
                        println!("registering migration: db_name: {}", db_name);
                        register_migration(db_name, override_version, &opt.rootdir)?;
                    }
                    RegisterActions::Seed {
                        db_name,
                        name,
                        override_version,
                    } => {
                        println!("registering seeds: db_name: {}", db_name);
                        register_seed(db_name, name, override_version, &opt.rootdir)?;
                    }
                    RegisterActions::DesignDoc {
                        db_name,
                        name,
                        override_version,
                        add
                    } => {
                        println!("registering design doc: db_name: {}", db_name);
                        // println!("add: {:?}", add);
                        let design_doc_name = name.clone();
                        if add.is_some() {
                            match add.unwrap() {
                                AddActions::View { name } => register_design_doc_view(db_name, design_doc_name, override_version, &opt.rootdir, name)?
                            };
                        } else {
                            register_design_doc(db_name, name, override_version, &opt.rootdir)?;
                        }
                    }
                    RegisterActions::Security { db_name } => {
                        println!("registering security: db_name: {}", db_name);
                        register_security(db_name, &opt.rootdir)?;
                    }
                },
                Actions::ShowLicense => {
                    println!("{}", LICENSE);
                }
            }
        }
    };
    Ok(())
}
