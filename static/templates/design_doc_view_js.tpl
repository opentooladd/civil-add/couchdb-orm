function (doc) {
  var ndoc = Object.assign({}, doc);
  ndoc.name = doc.name.find(function (n) {
    return n.language == "FR"
  }).data;
  ndoc.description = doc.description.find(function (n) {
    return n.language == "FR"
  }).data;
  ndoc.effect = doc.effect.find(function (n) {
    return n.language == "FR"
  }).data;
  ndoc.heightened = doc.heightened.find(function (n) {
    return n.language == "FR"
  }).data;
  ndoc.traits = doc.traits.map(function (t) {
    var trait = Object.assign({}, t);
    trait.name = t.name.find(function (n) {
      return n.language == "FR"
    }).data
    trait.description = t.description.find(function (n) {
      return n.language == "FR"
    }).data;
    return trait;
  });
  ndoc.targets = doc.targets.map(function (t) {
    var target = {};
    target.number = t.Creature[0];
    target.description = t.Creature[1].find(function (n) {
      return n.language == "FR"
    }).data;
    return target;
  });
  emit("fr", ndoc);
}