{} => {{
    let client = CouchDBClient::new(None);
    let list = {}AvailableDesignDocs::to_list();
    if list.len() == 0 {{
      return Ok(println!("no available design_doc files. Please register a design_doc first"))
   }}
    let design_docs_to_perform: usize = Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Which design_docs to perform ?:")
        .items(&list)
        .interact()?;
    match design_docs_to_perform {{
        {}
        s => {{
            println!("{{}} is not an existing design_doc", s);
        }}
    }}
}}
