use couchdb_orm::client::couchdb::actions::db::{{
    seed::{{
        seed_db
    }},
    backup::{{
        backup_db
    }}
}};
use couchdb_orm::awc::Client;
use crate::schemas::{}::{}::*;

// Here you can implement the data to be injected into the
// database.
pub fn get_seed() -> Vec<YourSchema> {{
    vec![]
}}

pub async fn execute_seed(client: &Client, host: &str) -> Result<bool, Box<dyn std::error::Error>> {{
        seed_db::<YourSchema>(client, "{}", get_seed(), host).await
}}


pub async fn execute_restore(client: &Client, data: couchdb_orm::serde_json::Value, host: &str) -> Result<bool, Box<dyn std::error::Error>> {{
        let t_data: Vec<YourSchema> = couchdb_orm::serde_json::from_value(data).unwrap();
        seed_db::<YourSchema>(client, "{}", t_data, host).await
}}

pub async fn execute_backup(client: &Client, seed_name: &str, host: &str) -> Result<bool, Box<dyn std::error::Error>> {{
        backup_db::<YourSchema>(client, "{}", seed_name, host).await
}}
