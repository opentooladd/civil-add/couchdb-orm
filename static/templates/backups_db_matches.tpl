{} => {{
    let client = CouchDBClient::new(None);
    let list = &{}AvailableSeeds::to_list();
    if list.len() == 0 {{
      return Ok(println!("no available seeds files. Please register a seed first"))
   }}
   let backup_to_perform: usize = if arguments.len() > 3 {{
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {{
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which backup to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }}
   }} else {{
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which backup to perform ?:")
           .items(&list)
           .interact()?
   }};
    match backup_to_perform {{
        {}
        s => {{
            println!("{{}} is not an existing backup", s);
        }}
    }}
}}
