couchdb-orm = "{}"
serde = "1.0.118"
actix-rt = "1.1.1" #actix-runtime

[lib]
name = "couchdb_orm_meta"
path = "src/lib.rs"

[[bin]]
name = "couchdb-orm-meta"
path = "src/main.rs"
