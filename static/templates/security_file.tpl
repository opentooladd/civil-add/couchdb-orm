use couchdb_orm::client::couchdb::actions::db::{{
    secure::{{
        secure_db
    }}
}};
use couchdb_orm::awc::Client;
use couchdb_orm::client::couchdb::models::Security;
use couchdb_orm::client::couchdb::models::SecurityObject;

pub fn new() -> Security {{
    Security {{
        admins: Some(SecurityObject {{
            names: vec!["".to_string()],
            roles: vec!["".to_string()]
        }}),
        members: None
    }}
}}


pub async fn create_security(client: &Client, host: &str) -> Result<bool, Box<dyn std::error::Error>> {{
        secure_db(client, "{}", new(), host).await
}}
