use couchdb_orm::client::couchdb::actions::db::{{
    migrate::{{
        migrate_db
    }}
}};
use couchdb_orm::awc::Client;
use crate::schemas::{}::{}::*;
use crate::schemas::{}::{}::*;

// Here you must implement a transition between you 2 schemas
// feel free to change imports
impl From<YourSrcSchema> for YourTargetSchema {{
    fn from(src: &YourSrcSchema) -> YourTargetSchema {{
        !unimplemented!()
    }}
}}

pub async fn execute_migration(client: &Client, host: &str) -> Result<bool, Box<dyn std::error::Error>> {{
        migrate_db::<YourSrcSchema, YourTargetSchema>(client, "{}", host).await
}}
