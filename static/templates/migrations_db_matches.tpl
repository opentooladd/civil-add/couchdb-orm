{} => {{
    let list = {}AvailableMigrations::to_list();
    if list.len() == 0 {{
      return Ok(println!("no available migration files. Please register a migration first"))
   }}
    let client = CouchDBClient::new(None);
    let migration_to_perform: usize = Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Which migration to perform ?:")
        .items(&list)
        .interact()?;
    match migration_to_perform {{
        {}
        s => {{
            println!("{{}} is not an existing migration", s);
        }}
    }}
}}
