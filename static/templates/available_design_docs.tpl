use couchdb_orm::client::couchdb::actions::design_doc::{
    create::create_design_doc
};
use couchdb_orm::awc::Client;
use couchdb_orm::structs::{ DesignDoc };

pub mod views;

pub fn get_design_doc() -> DesignDoc {{
    let views = views::AvailableViews::get_views();
    DesignDoc::new(
        views
    )
}}

pub async fn create(
    client: &Client,
    db_name: &str,
    host: &str,
    design_doc_name: &str,
) -> Result<bool, Box<dyn std::error::Error>> {{
    create_design_doc(
        client,
        db_name,
        host,
        design_doc_name,
        get_design_doc(),
    ).await
}}



