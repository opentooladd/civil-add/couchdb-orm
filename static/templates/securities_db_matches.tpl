{} => {{
    let client = CouchDBClient::new(None);
    let list = {}AvailableSecurities::to_list();
    if list.len() == 0 {{
      return Ok(println!("no available security files. Please register a security first"))
   }}
    let securities_to_perform: usize = if arguments.len() > 3 {{
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {{
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which security to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }}
   }} else {{
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which security to perform ?:")
           .items(&list)
           .interact()?
   }};
    match securities_to_perform {{
        {}
        s => {{
            println!("{{}} is not an existing security", s);
        }}
    }}
}}
