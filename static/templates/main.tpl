extern crate couchdb_orm;
extern crate serde;

use couchdb_orm::actix_rt;
use couchdb_orm::client::couchdb::CouchDBClient;
use couchdb_orm::dialoguer::{{
    Select,
    Confirm,
    theme::ColorfulTheme
}};

pub mod schemas;
pub mod migrations;
pub mod seeds;
pub mod securities;
pub mod design_docs;

// migrations
{}
{}
// seeds
{}
{}
// securities
{}
{}
// design_docs
{}
{}

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {{
    {}
    let available_actions: [&str;6] = ["migrate", "seed", "secure", "backup", "restore", "design_doc_create"];

    let arguments: Vec<String> = std::env::args().collect();

    let action_to_perform: usize = if arguments.len() > 1 {{
      match available_actions.iter().position(|a| a == &arguments[1]) {{
         Some(i) => i,
         None => {{
            Select::with_theme(&ColorfulTheme::default())
              .with_prompt("Which action to perform:")
              .items(&available_actions)
              .interact()?
         }}
      }}
    }} else {{
      Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Which action to perform:")
        .items(&available_actions)
        .interact()?
    }};

    match action_to_perform {{
        0 => {{
            let db_for_migration: usize = if arguments.len() > 2 {{
                match migrations_available_databases.iter().position(|a| a == &arguments[2]) {{
                     Some(i) => i,
                     None => {{
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to perform migration ?:")
                            .items(&migrations_available_databases)
                            .interact()?
                     }}
                  }}
            }} else {{
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to perform migration ?:")
                .items(&migrations_available_databases)
                .interact()?
            }};

            match db_for_migration {{
                {}
                _ => {{
                    println!("{{}} database does not exist", db_for_migration);
                }}
            }}
        }}
        1 => {{
            let db_for_seeds: usize = if arguments.len() > 2 {{
                match seeds_available_databases.iter().position(|a| a == &arguments[2]) {{
                     Some(i) => i,
                     None => {{
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to perform seeds ?:")
                            .items(&seeds_available_databases)
                            .interact()?
                     }}
                  }}
            }} else {{
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to perform seeds ?:")
                .items(&seeds_available_databases)
                .interact()?
            }};
            let db_name = seeds_available_databases[db_for_seeds];
            let client = CouchDBClient::new(None);
            let db_status: couchdb_orm::client::couchdb::responses::db_status::DbStatus =
                client.get_db_status(db_name).await?;

            if db_status.doc_count > 0 {{
                if Confirm::with_theme(&ColorfulTheme::default())
                    .with_prompt("Do you want to clear the database ?")
                    .interact()?
                {{
                    println!("ok let's clear it !");

                    client.delete_all_docs(&db_name)
                    .await?;
                }} else {{
                    panic!("aborting");
                }}
            }}

            match db_for_seeds {{
                {}
                _ => {{
                    println!("{{}} database does not exist", db_for_seeds);
                }}
            }}
        }}
        2 => {{
            let db_for_security: usize = if arguments.len() > 2 {{
                match securities_available_databases.iter().position(|a| a == &arguments[2]) {{
                     Some(i) => i,
                     None => {{
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to create security ?:")
                            .items(&securities_available_databases)
                            .interact()?
                     }}
                  }}
            }} else {{
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to create security ?:")
                .items(&securities_available_databases)
                .interact()?
            }};

            match db_for_security {{
                {}
                _ => {{
                    println!("{{}} database does not exist", db_for_security);
                }}
            }}
        }}
        3 => {{
            let db_for_backups: usize = if arguments.len() > 2 {{
                match seeds_available_databases.iter().position(|a| a == &arguments[2]) {{
                     Some(i) => i,
                     None => {{
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to perform backups ?:")
                            .items(&seeds_available_databases)
                            .interact()?
                     }}
                  }}
            }} else {{
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to perform backups ?:")
                .items(&seeds_available_databases)
                .interact()?
            }};

            match db_for_backups {{
                {}
                _ => {{
                    println!("{{}} database does not exist", db_for_backups);
                }}
            }}
        }}
        4 => {{
            let db_for_restore: usize = if arguments.len() > 2 {{
                match seeds_available_databases.iter().position(|a| a == &arguments[2]) {{
                     Some(i) => i,
                     None => {{
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to perform restore?:")
                            .items(&seeds_available_databases)
                            .interact()?
                     }}
                  }}
            }} else {{
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to perform restore?:")
                .items(&seeds_available_databases)
                .interact()?
            }};
            let db_name = seeds_available_databases[db_for_restore];
            let client = CouchDBClient::new(None);
            let db_status: couchdb_orm::client::couchdb::responses::db_status::DbStatus = client.get_db_status(db_name).await?;

            if db_status.doc_count > 0 {{
                if Confirm::with_theme(&ColorfulTheme::default())
                    .with_prompt("Do you want to clear the database ?")
                    .interact()?
                {{
                    println!("ok let's clear it !");

                    client.delete_all_docs(&db_name)
                    .await?;
                }} else {{
                    panic!("aborting");
                }}
            }}

            match db_for_restore {{
                {}
                _ => {{
                    println!("{{}} database does not exist", db_for_restore);
                }}
            }}
        }}
        5 => {{
            let db_for_design_doc: usize = if arguments.len() > 2 {{
                match design_docs_available_databases.iter().position(|a| a == &arguments[2]) {{
                     Some(i) => i,
                     None => {{
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to create design doc?:")
                            .items(&design_docs_available_databases)
                            .interact()?
                     }}
                  }}
            }} else {{
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to create design doc?:")
                .items(&design_docs_available_databases)
                .interact()?
            }};
            let db_name = design_docs_available_databases[db_for_design_doc];
            let client = CouchDBClient::new(None);

            match db_for_design_doc {{
                {}
                _ => {{
                    println!("{{}} database does not exist", db_for_design_doc);
                }}
            }}
        }}
        _ => {{
            println!("not an action")
        }}
    }}

    Ok(())
}}
