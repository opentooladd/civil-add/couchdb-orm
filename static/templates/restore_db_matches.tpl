{} => {{
    let list = {}AvailableSeeds::to_list();
   if list.len() == 0 {{
      return Ok(println!("no available restore files. Please register a seed first"))
   }}
   let restore_to_perform: usize = if arguments.len() > 3 {{
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {{
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which restore to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }}
   }} else {{
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which restore to perform ?:")
           .items(&list)
           .interact()?
   }};
    let root_path = std::env::current_dir().unwrap();
    let seed_path = format!("backups/{}/{{}}", &list[restore_to_perform]);
    let available_datas: Vec<String> = std::fs::read_dir(root_path.join(&seed_path)).unwrap()
    .map(|r| r.unwrap().path().to_str().unwrap().to_string())
    .collect();
    let data_to_restore: usize = Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Which restore to perform ?:")
        .items(&available_datas)
        .interact()?;
    match restore_to_perform {{
        {}
        s => {{
            println!("{{}} is not an existing backup", s);
        }}
    }}
}}
