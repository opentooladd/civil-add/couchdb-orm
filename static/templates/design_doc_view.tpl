use std::collections::HashMap;


pub fn get_view() -> HashMap<String, String> {{
  let mut view: HashMap<String, String> = HashMap::new();

  view.insert(
    "map".to_string(),
    String::from(include_str!("./js/{}.js")),
  );

  view
}}

pub fn get_view_key() -> String {{
  "fr".to_string()
}}