{} => {{
   let files_in_data = std::fs::read_dir(&available_datas[data_to_restore]).unwrap();
   for entry in files_in_data {{
      // Open the file in read-only mode with buffer.
      let file = std::fs::File::open(entry.unwrap().path())?;
      let reader = std::io::BufReader::new(file);

      let u: couchdb_orm::serde_json::Value = couchdb_orm::serde_json::from_reader(reader)?;
      {}::execute_restore(
          &client.client,
          u,
          &client.host(),
      ).await?;
   }}
}}
