# syntax=docker/dockerfile:experimental
from rustlang/rust:nightly
ENV HOME=/usr/src
WORKDIR $HOME/app

COPY src/ $HOME/app/src
COPY static/ $HOME/app/static
COPY tests/ $HOME/app/tests
COPY _meta/ $HOME/app/_meta
COPY Cargo.toml $HOME/app
COPY LICENSE $HOME/app
COPY LICENSE_DISCLAIMER $HOME/app
COPY tarpaulin.toml $HOME/app
COPY Cargo.lock $HOME/app

RUN --mount=type=cache,target=/usr/local/cargo/registry \
    --mount=type=cache,target=/usr/src/app/target \
    cargo build --release
