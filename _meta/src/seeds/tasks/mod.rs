pub mod seed_1661681829_seed_new_tasks;
pub mod seed_1661680185_base_seed;

pub enum AvailableSeeds {
    seed_1661681829_seed_new_tasks,
seed_1661680185_base_seed
}

impl AvailableSeeds {
    pub fn to_list() -> Vec<&'static str> {
        vec!(
            "seed_1661681829_seed_new_tasks",
"seed_1661680185_base_seed"
        )
    }
}
