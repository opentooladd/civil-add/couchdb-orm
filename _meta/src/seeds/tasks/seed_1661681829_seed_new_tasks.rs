use couchdb_orm::client::couchdb::actions::db::{
    seed::{
        seed_db
    },
    backup::{
        backup_db
    }
};
use std::collections::HashMap;
use couchdb_orm::awc::Client;
use crate::schemas::tasks::schema_1661681370::*;

// Here you can implement the data to be injected into the
// database.
pub fn get_seed() -> Vec<Task> {
    (1..10)
        .map(|n: i32| {
            let mut team: Team = HashMap::new();
            team.insert(String::from("Ciro"), TeamRole::Owner);
            return Task {
                id: Some(format!("task-id-{}", n)),
                rev: None,
                title: format!("Task {}", n),
                status: Status::Draft,
                description: String::from(""),
                team,
            };
        })
        .collect()
}

pub async fn execute_seed(client: &Client, host: &str) -> Result<bool, Box<dyn std::error::Error>> {
        seed_db::<Task>(client, "tasks", get_seed(), host).await
}


pub async fn execute_restore(client: &Client, data: couchdb_orm::serde_json::Value, host: &str) -> Result<bool, Box<dyn std::error::Error>> {
        let t_data: Vec<Task> = couchdb_orm::serde_json::from_value(data).unwrap();
        seed_db::<Task>(client, "tasks", t_data, host).await
}

pub async fn execute_backup(client: &Client, seed_name: &str, host: &str) -> Result<bool, Box<dyn std::error::Error>> {
        backup_db::<Task>(client, "tasks", seed_name, host).await
}
