pub mod from_schema_1661680133_to_schema_1661681370;

pub enum AvailableMigrations {
    from_schema_1661680133_to_schema_1661681370
}

impl AvailableMigrations {
    pub fn to_list() -> Vec<&'static str> {
        vec!(
            "from_schema_1661680133_to_schema_1661681370"
        )
    }
}
