use crate::schemas::tasks::schema_1661680133::{Status as oldStatus, Task as OldTask};
use crate::schemas::tasks::schema_1661681370::{Status, Task, Team, TeamRole};
use couchdb_orm::awc::Client;
use couchdb_orm::client::couchdb::actions::db::migrate::migrate_db;
use std::collections::HashMap;

// Here you must implement a transition between you 2 schemas
// feel free to change imports
impl From<OldTask> for Task {
    fn from(src: OldTask) -> Task {
        let mut team: Team = HashMap::new();
        team.insert(src.owner.clone(), TeamRole::Owner);
        let status = match src.status {
            oldStatus::Draft => Status::Draft,
            oldStatus::Ready => Status::Ready,
            oldStatus::Completed => Status::Completed,
        };
        Task {
            id: src.id.clone(),
            rev: src.rev.clone(),
            title: src.name.clone(),
            description: "".to_string(),
            status,
            team,
        }
    }
}

pub async fn execute_migration(
    client: &Client,
    host: &str,
) -> Result<bool, Box<dyn std::error::Error>> {
    migrate_db::<Task, OldTask>(client, "tasks", host).await
}
