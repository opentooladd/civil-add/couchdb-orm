extern crate couchdb_orm;
extern crate serde;

use couchdb_orm::actix_rt;
use couchdb_orm::client::couchdb::CouchDBClient;
use couchdb_orm::dialoguer::{
    Select,
    Confirm,
    theme::ColorfulTheme
};

pub mod schemas;
pub mod migrations;
pub mod seeds;
pub mod securities;

// migrations
use migrations::tasks::{ AvailableMigrations as TasksAvailableMigrations };

use migrations::test::{ AvailableMigrations as TestAvailableMigrations };

use migrations::tasks::{
    from_schema_1661680133_to_schema_1661681370,

};

use migrations::test::{
    
};

// seeds
use seeds::tasks::{ AvailableSeeds as TasksAvailableSeeds };

use seeds::test::{ AvailableSeeds as TestAvailableSeeds };

use seeds::tasks::{
    seed_1661681829_seed_new_tasks,
seed_1661680185_base_seed,

};

use seeds::test::{
    
};

// securities
use securities::tasks::{ AvailableSecurities as TasksAvailableSecurities };

use securities::test::{ AvailableSecurities as TestAvailableSecurities };

use securities::tasks::{
    
};

use securities::test::{
    
};


#[actix_rt::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let migrations_available_databases: [&str;2] = [
        "tasks",
"test",
    ];
let seeds_available_databases: [&str;2] = [
        "tasks",
"test",
    ];
let securities_available_databases: [&str;2] = [
        "tasks",
"test",
    ];

    let available_actions: [&str;5] = ["migrate", "seed", "secure", "backup", "restore"];

    let arguments: Vec<String> = std::env::args().collect();

    let action_to_perform: usize = if arguments.len() > 1 {
      match available_actions.iter().position(|a| a == &arguments[1]) {
         Some(i) => i,
         None => {
            Select::with_theme(&ColorfulTheme::default())
              .with_prompt("Which action to perform:")
              .items(&available_actions)
              .interact()?
         }
      }
    } else {
      Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Which action to perform:")
        .items(&available_actions)
        .interact()?
    };

    match action_to_perform {
        0 => {
            let db_for_migration: usize = if arguments.len() > 2 {
                match migrations_available_databases.iter().position(|a| a == &arguments[2]) {
                     Some(i) => i,
                     None => {
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to perform migration ?:")
                            .items(&migrations_available_databases)
                            .interact()?
                     }
                  }
            } else {
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to perform migration ?:")
                .items(&migrations_available_databases)
                .interact()?
            };

            match db_for_migration {
                0 => {
    let list = TasksAvailableMigrations::to_list();
    if list.len() == 0 {
      return Ok(println!("no available migration files. Please register a migration first"))
   }
    let client = CouchDBClient::new(None);
    let migration_to_perform: usize = Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Which migration to perform ?:")
        .items(&list)
        .interact()?;
    match migration_to_perform {
        0 => {
   from_schema_1661680133_to_schema_1661681370::execute_migration(&client.client, &client.host()).await?; 
}

        s => {
            println!("{} is not an existing migration", s);
        }
    }
}

1 => {
    let list = TestAvailableMigrations::to_list();
    if list.len() == 0 {
      return Ok(println!("no available migration files. Please register a migration first"))
   }
    let client = CouchDBClient::new(None);
    let migration_to_perform: usize = Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Which migration to perform ?:")
        .items(&list)
        .interact()?;
    match migration_to_perform {
        
        s => {
            println!("{} is not an existing migration", s);
        }
    }
}

                _ => {
                    println!("{} database does not exist", db_for_migration);
                }
            }
        }
        1 => {
            let db_for_seeds: usize = if arguments.len() > 2 {
                match seeds_available_databases.iter().position(|a| a == &arguments[2]) {
                     Some(i) => i,
                     None => {
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to perform seeds ?:")
                            .items(&seeds_available_databases)
                            .interact()?
                     }
                  }
            } else {
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to perform seeds ?:")
                .items(&seeds_available_databases)
                .interact()?
            };
            let db_name = seeds_available_databases[db_for_seeds];
            let client = CouchDBClient::new(None);
            let db_status: couchdb_orm::client::couchdb::responses::db_status::DbStatus =
                client.get_db_status(db_name).await?;

            if db_status.doc_count > 0 {
                if Confirm::with_theme(&ColorfulTheme::default())
                    .with_prompt("Do you want to clear the database ?")
                    .interact()?
                {
                    println!("ok let's clear it !");

                    client.delete_all_docs(&db_name)
                    .await?;
                } else {
                    panic!("aborting");
                }
            }

            match db_for_seeds {
                0 => {
    let list = TasksAvailableSeeds::to_list();
    if list.len() == 0 {
      return Ok(println!("no available seed files. Please register a seed first"))
   }
   let seed_to_perform: usize = if arguments.len() > 3 {
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which seed to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }
   } else {
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which seed to perform ?:")
           .items(&list)
           .interact()?
   };
    match seed_to_perform {
        0 => {
   seed_1661681829_seed_new_tasks::execute_seed(&client.client, &client.host()).await?; 
}
1 => {
   seed_1661680185_base_seed::execute_seed(&client.client, &client.host()).await?; 
}

        s => {
            println!("{} is not an existing seed", s);
        }
    }
}

1 => {
    let list = TestAvailableSeeds::to_list();
    if list.len() == 0 {
      return Ok(println!("no available seed files. Please register a seed first"))
   }
   let seed_to_perform: usize = if arguments.len() > 3 {
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which seed to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }
   } else {
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which seed to perform ?:")
           .items(&list)
           .interact()?
   };
    match seed_to_perform {
        
        s => {
            println!("{} is not an existing seed", s);
        }
    }
}

                _ => {
                    println!("{} database does not exist", db_for_seeds);
                }
            }
        }
        2 => {
            let db_for_security: usize = if arguments.len() > 2 {
                match securities_available_databases.iter().position(|a| a == &arguments[2]) {
                     Some(i) => i,
                     None => {
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to create security ?:")
                            .items(&securities_available_databases)
                            .interact()?
                     }
                  }
            } else {
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to create security ?:")
                .items(&securities_available_databases)
                .interact()?
            };

            match db_for_security {
                0 => {
    let client = CouchDBClient::new(None);
    let list = TasksAvailableSecurities::to_list();
    if list.len() == 0 {
      return Ok(println!("no available security files. Please register a security first"))
   }
    let securities_to_perform: usize = if arguments.len() > 3 {
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which security to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }
   } else {
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which security to perform ?:")
           .items(&list)
           .interact()?
   };
    match securities_to_perform {
        
        s => {
            println!("{} is not an existing security", s);
        }
    }
}

1 => {
    let client = CouchDBClient::new(None);
    let list = TestAvailableSecurities::to_list();
    if list.len() == 0 {
      return Ok(println!("no available security files. Please register a security first"))
   }
    let securities_to_perform: usize = if arguments.len() > 3 {
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which security to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }
   } else {
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which security to perform ?:")
           .items(&list)
           .interact()?
   };
    match securities_to_perform {
        
        s => {
            println!("{} is not an existing security", s);
        }
    }
}

                _ => {
                    println!("{} database does not exist", db_for_security);
                }
            }
        }
        3 => {
            let db_for_backups: usize = if arguments.len() > 2 {
                match seeds_available_databases.iter().position(|a| a == &arguments[2]) {
                     Some(i) => i,
                     None => {
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to perform backups ?:")
                            .items(&seeds_available_databases)
                            .interact()?
                     }
                  }
            } else {
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to perform backups ?:")
                .items(&seeds_available_databases)
                .interact()?
            };

            match db_for_backups {
                0 => {
    let client = CouchDBClient::new(None);
    let list = &TasksAvailableSeeds::to_list();
    if list.len() == 0 {
      return Ok(println!("no available seeds files. Please register a seed first"))
   }
   let backup_to_perform: usize = if arguments.len() > 3 {
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which backup to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }
   } else {
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which backup to perform ?:")
           .items(&list)
           .interact()?
   };
    match backup_to_perform {
        0 => {
   seed_1661681829_seed_new_tasks::execute_backup(&client.client, &list[0], &client.host()).await?; 
}
1 => {
   seed_1661680185_base_seed::execute_backup(&client.client, &list[1], &client.host()).await?; 
}

        s => {
            println!("{} is not an existing backup", s);
        }
    }
}

1 => {
    let client = CouchDBClient::new(None);
    let list = &TestAvailableSeeds::to_list();
    if list.len() == 0 {
      return Ok(println!("no available seeds files. Please register a seed first"))
   }
   let backup_to_perform: usize = if arguments.len() > 3 {
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which backup to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }
   } else {
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which backup to perform ?:")
           .items(&list)
           .interact()?
   };
    match backup_to_perform {
        
        s => {
            println!("{} is not an existing backup", s);
        }
    }
}

                _ => {
                    println!("{} database does not exist", db_for_backups);
                }
            }
        }
        4 => {
            let db_for_restore: usize = if arguments.len() > 2 {
                match seeds_available_databases.iter().position(|a| a == &arguments[2]) {
                     Some(i) => i,
                     None => {
                        Select::with_theme(&ColorfulTheme::default())
                            .with_prompt("Which DB to perform restore?:")
                            .items(&seeds_available_databases)
                            .interact()?
                     }
                  }
            } else {
                Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Which DB to perform restore?:")
                .items(&seeds_available_databases)
                .interact()?
            };
            let db_name = seeds_available_databases[db_for_restore];
            let client = CouchDBClient::new(None);
            let db_status: couchdb_orm::client::couchdb::responses::db_status::DbStatus = client.get_db_status(db_name).await?;

            if db_status.doc_count > 0 {
                if Confirm::with_theme(&ColorfulTheme::default())
                    .with_prompt("Do you want to clear the database ?")
                    .interact()?
                {
                    println!("ok let's clear it !");

                    client.delete_all_docs(&db_name)
                    .await?;
                } else {
                    panic!("aborting");
                }
            }

            match db_for_restore {
                0 => {
    let list = TasksAvailableSeeds::to_list();
   if list.len() == 0 {
      return Ok(println!("no available restore files. Please register a seed first"))
   }
   let restore_to_perform: usize = if arguments.len() > 3 {
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which restore to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }
   } else {
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which restore to perform ?:")
           .items(&list)
           .interact()?
   };
    let root_path = std::env::current_dir().unwrap();
    let seed_path = format!("backups/tasks/{}", &list[restore_to_perform]);
    let available_datas: Vec<String> = std::fs::read_dir(root_path.join(&seed_path)).unwrap()
    .map(|r| r.unwrap().path().to_str().unwrap().to_string())
    .collect();
    let data_to_restore: usize = Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Which restore to perform ?:")
        .items(&available_datas)
        .interact()?;
    match restore_to_perform {
        0 => {
   let files_in_data = std::fs::read_dir(&available_datas[data_to_restore]).unwrap();
   for entry in files_in_data {
      // Open the file in read-only mode with buffer.
      let file = std::fs::File::open(entry.unwrap().path())?;
      let reader = std::io::BufReader::new(file);

      let u: couchdb_orm::serde_json::Value = couchdb_orm::serde_json::from_reader(reader)?;
      seed_1661681829_seed_new_tasks::execute_restore(
          &client.client,
          u,
          &client.host(),
      ).await?;
   }
}
1 => {
   let files_in_data = std::fs::read_dir(&available_datas[data_to_restore]).unwrap();
   for entry in files_in_data {
      // Open the file in read-only mode with buffer.
      let file = std::fs::File::open(entry.unwrap().path())?;
      let reader = std::io::BufReader::new(file);

      let u: couchdb_orm::serde_json::Value = couchdb_orm::serde_json::from_reader(reader)?;
      seed_1661680185_base_seed::execute_restore(
          &client.client,
          u,
          &client.host(),
      ).await?;
   }
}

        s => {
            println!("{} is not an existing backup", s);
        }
    }
}

1 => {
    let list = TestAvailableSeeds::to_list();
   if list.len() == 0 {
      return Ok(println!("no available restore files. Please register a seed first"))
   }
   let restore_to_perform: usize = if arguments.len() > 3 {
       match list
           .iter()
           .position(|a| a == &arguments[3])
       {
           Some(i) => i,
           None => Select::with_theme(&ColorfulTheme::default())
               .with_prompt("Which restore to perform ?:")
               .items(&securities_available_databases)
               .interact()?,
       }
   } else {
       Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Which restore to perform ?:")
           .items(&list)
           .interact()?
   };
    let root_path = std::env::current_dir().unwrap();
    let seed_path = format!("backups/test/{}", &list[restore_to_perform]);
    let available_datas: Vec<String> = std::fs::read_dir(root_path.join(&seed_path)).unwrap()
    .map(|r| r.unwrap().path().to_str().unwrap().to_string())
    .collect();
    let data_to_restore: usize = Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Which restore to perform ?:")
        .items(&available_datas)
        .interact()?;
    match restore_to_perform {
        
        s => {
            println!("{} is not an existing backup", s);
        }
    }
}

                _ => {
                    println!("{} database does not exist", db_for_restore);
                }
            }
        }
        _ => {
            println!("not an action")
        }
    }

    Ok(())
}
