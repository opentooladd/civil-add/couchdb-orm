#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub enum Status {
    Draft,
    Ready,
    Completed,
}

#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct Task {
    #[serde(rename = "_id")]
    pub id: Option<String>,
    #[serde(rename = "_rev")]
    pub rev: Option<String>,
    pub name: String,
    pub status: Status,
    pub owner: String,
}

impl Task {
    pub fn new(name: String, owner: String) -> Self {
        Task {
            id: Some("test-id".to_string()),
            rev: None,
            name,
            status: Status::Draft,
            owner,
        }
    }
}
