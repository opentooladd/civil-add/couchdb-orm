use std::collections::HashMap;

#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub enum Status {
    Draft,
    Ready,
    Completed,
}

#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub enum TeamRole {
    Owner,
    Contributor,
}

pub type Team = HashMap<String, TeamRole>;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Task {
    #[serde(rename = "_id")]
    pub id: Option<String>,
    #[serde(rename = "_rev")]
    pub rev: Option<String>,
    pub title: String,
    pub description: String,
    pub status: Status,
    pub team: Team,
}

impl Task {
    pub fn new(title: String, description: Option<String>, team: Team) -> Self {
        Task {
            id: None,
            rev: None,
            title,
            description: description.unwrap_or_default(),
            status: Status::Draft,
            team,
        }
    }
}
