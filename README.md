# CouchDB ORM

A CouchDB ORM built in Rust with Actix runtime and web client.

## @TODO

- [x] Network
  - [x] Add chunking to request to not overflow memory
- [x] seeding
    - [x] Add Seeding capacity from JSON ?
    - [x] Add Seeding capacity from rust models
- [x] Finish migrate_db tasks in `clients/couchdb/actions/db/migrate`
    - [x] actually execute the migration
    - [x] Pagination and limits (if big database, dont store everyting in memory, it will break)
    - [ ] Check schema entry with database schema
- [x] clean database
- [x] Security
  - [x] Management via model
- [x] Backup
  - [x] JSON associated to a seed
- [x] Restore
  - [x] Link backups to seeding file
- [ ] View
  - [ ] Views via models
  - [ ] Design Docs via models
- [ ] Test creation of files and folder structure
  - [x] utils
  - [x] register schema
  - [ ] migrations
  - [ ] seeding
  - [ ] backups
  - [ ] restore
- [/] tests clients actions
- [/] Better separation between lib and bin of couchdb-orm
- [ ] Integration use case testing

## Commands

> Every Database action executed on a couchdb instance needs some authentication.<br>
> The CLI will search for a `.env` file at the root of the execution folder.<br>
> If not present it will rely on env variables.<br>
> If there are no variables, it will crash before executing anything.<br>
> Copy the `.env.tpl` file at the root to have a working base and configure your
> instance of couchdb if it is not the default coming with this project.

### `couchdb-orm db`
Launch actions on a specific database.
see `--help` for available actions

#### `couchdb-orm db create <db-names>`
Create X databases based on name (comma separated)

#### `couchdb-orm db delete <db-names>`
Delete X databases based on name (comma separated)

#### `couchdb-orm db migrate`
Performs a migration on a given table.
Some migrations must be registered.
Interaction: `Space` to select, `Enter` to validate

### `couchdb-orm register`
Register something in the system.
see `--help` for available actions

### `couchdb-orm register schema <path> <db-name>`
Register a schema and version's it.
`<path>` is the path to the file containing the model and all it's dependencies.
`<db-name>` is the database for which the schema should be registered.

### `couchdb-orm register migration <db-name>`
Register a migration based on 2 available schemas.
`<db-name>` is the database for which the migration should be registered.
Interaction: `Space` to select, `Enter` to validate

## Dev

### Dependencies

- [Rust](https://www.rust-lang.org) >= 1.50
  - For the moment, needs Rust Nightly to be built for features:
    - `#![feature(proc_macro_span)`
- [Docker](https://www.docker.com/get-started): For `couchdb` service for tests
- [Docker-Compose](https://docs.docker.com/compose/): For `couchdb` service for tests 

### Make Commands

#### `make build-base-image`
Will build the base `couchdb-docker` image needed for the project.
Useful for testing and dev

#### `make start-couchdb`
Launch the couchdb instance with the `docker-compose.yml`

#### `make test`
Launch the tests with cargo.
For the moment its a single thread because it is needed to clean the test folder
everytime to test filesystem interaction.

#### `make coverage`
Uses [Tarpaulin](https://github.com/xd009642/tarpaulin) to make HTML coverage 
in the `coverage` directory

#### `make install`
Install the package as a CLI tool to use with `couchdb-orm` in command-line

### Links

- [awc](https://docs.rs/awc/2.0.3/awc/index.html): HTTP client
- [serde](https://docs.serde.rs/serde): serialize and deserialize
- [serde_json](https://docs.serde.rs/serde_json/): serialize and deserialize json type
- [actix-rt](https://docs.rs/actix-rt/1.1.1/actix_rt/index.html): Actix runtime for asynchronous code
- [thiserror](https://docs.rs/thiserror/1.0.22/thiserror/): Error derive
- [paw](https://github.com/rust-cli/paw): CLI utility for formatting and colors
- [structopt](https://docs.rs/structopt/0.3.21/structopt/): CLI args parser utility
- [dialoguer](https://docs.rs/dialoguer/0.7.1/dialoguer/index.html): CLI interaction library
- [console](https://docs.rs/console/0.13.0/console/): CLI Terminal writer library
- [indicatif](https://docs.rs/indicatif/0.15.0/indicatif/): CLI progress constructor library
- [chrono](https://docs.rs/chrono/0.4.19/chrono/index.html): Time library

### Tests

- [actix-web](https://actix.rs/docs/testing/)
- [wiremock](https://github.com/LukeMathWalker/wiremock-rs)
- [Tarpaulin](https://github.com/xd009642/tarpaulin)

### Docs

- Actix
    - https://actix.rs/book/actix/sec-1-getting-started.html
- CLI Args
    - https://rust-cli.github.io/book/tutorial/cli-args.html

## License

```
Copyright (C) 2020-2023  OpenToolAdd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
contact: contact@tool-add.com

```
