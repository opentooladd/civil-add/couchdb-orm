use assert_cmd::prelude::*; // Add methods on commands
use predicates::prelude::*; // Used for writing assertions
use std::process::Command; // Run programs

#[test]
fn main_help_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("couchdb-orm")?;

    cmd.arg("--help");
    cmd.assert().success().stdout(predicate::str::contains(
        r#"
USAGE:
    couchdb-orm [OPTIONS] <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --client <client>    The client to use for the requests [default: couchdb]
    -r, --root <rootdir>     the root directory to store the _meta informations [default: .]

SUBCOMMANDS:
    db              Execute Database actions
    help            Prints this message or the help of the given subcommand(s)
    register        Execute register actions to manipulate schemas for migrations and other
    show-license    Show license"#,
    ));

    Ok(())
}
