use assert_cmd::prelude::*; // Add methods on commands
use predicates::prelude::*; // Used for writing assertions
use std::process::Command; // Run programs

#[test]
fn db_integration_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("couchdb-orm")?;

    cmd.arg("db");
    cmd.assert().failure().stderr(predicate::str::contains(
        r#"
USAGE:
    couchdb-orm db <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    all-docs       Get all documents
    backup         Backups the document as a seed and a json file
    create         Create a database with name
    delete         Delete a database with name
    delete-docs    Delete all docs from a database
    help           Prints this message or the help of the given subcommand(s)
    migrate        migrate
    restore        Restore the document based on JSON backups
    secure         secure
    seed           seed"#,
    ));

    Ok(())
}
