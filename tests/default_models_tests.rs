pub mod default;
use default::models::task_old::Task;

#[test]
fn test_model() {
    let task: Task = Task::new("test".to_string(), "test_owner".to_string());
    assert_eq!(task.name, "test");
    assert_eq!(task.owner, "test_owner");
}
