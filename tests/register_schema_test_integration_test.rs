use assert_cmd::prelude::*; // Add methods on commands
use couchdb_orm::tests::utils::*;
use predicates::prelude::*; // Used for writing assertions
use std::path::PathBuf;
use std::process::Command; // Run programs

#[test]
fn register_integration_error_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("couchdb-orm")?;

    cmd.arg("register");
    cmd.assert()
        .failure()
        .stderr(predicate::str::contains(r#"
USAGE:
    couchdb-orm register <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    db           Register a db This gives automatisation of initialization
    help         Prints this message or the help of the given subcommand(s)
    migration    Register a migration based on the registered schemas
    schema       Register a schema a path {arg} The schema must be serializable, deserializable with serde
    security     Register a security object for a db
    seed         Register a seed based on the registered schemas"#));

    Ok(())
}

#[test]
fn register_schema_integration_no_path_error_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("couchdb-orm")?;

    cmd.arg("register");
    cmd.arg("schema");
    cmd.assert().failure().stderr(predicate::str::contains(
        r#"error: The following required arguments were not provided:
    <path>
    <db-name>

USAGE:
    couchdb-orm register schema [FLAGS] <path> <db-name>

For more information try --help"#,
    ));

    Ok(())
}

#[test]
fn register_schema_integration_no_db_error_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("couchdb-orm")?;

    cmd.arg("register");
    cmd.arg("schema");
    cmd.arg("test_file");
    cmd.assert().failure().stderr(predicate::str::contains(
        r#"error: The following required arguments were not provided:
    <db-name>

USAGE:
    couchdb-orm register schema [FLAGS] <path> <db-name>

For more information try --help"#,
    ));

    Ok(())
}

#[test]
fn register_schema_integration_no_file_error_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("couchdb-orm")?;

    cmd.arg("register");
    cmd.arg("schema");
    cmd.arg("test_file");
    cmd.arg("tasks");
    cmd.assert()
        .failure()
        .stderr(predicate::str::contains(r#"Error: RegisterSchemaError("register_schema > read schema content error: No such file or directory (os error 2) -> test_file"#));

    Ok(())
}

#[test]
fn register_schema_integration_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("couchdb-orm")?;

    create_tests_folder();

    let root_path = create_test_dir("register_schema_integration");

    let content: &str = "this is my content";
    let content2: &str = "this is my content2";
    let filepath: PathBuf = root_path.join("test_model");
    let filepath2: PathBuf = root_path.join("test_model_2");
    std::fs::write(&filepath, &content).unwrap();
    std::fs::write(&filepath2, &content2).unwrap();

    let db_name: &str = "tasks";

    cmd.arg("-r");
    cmd.arg(root_path);
    cmd.arg("register");
    cmd.arg("schema");
    cmd.arg(filepath.to_str().unwrap());
    cmd.arg(db_name);
    cmd.assert()
        .success()
        .stdout(predicate::str::contains(format!(
            r#"registering schema: path: "{}", db_name: tasks"#,
            filepath.display()
        )));

    Ok(())
}

#[test]
fn register_schema_integration_same_version_error_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("couchdb-orm")?;

    create_tests_folder();

    let root_path = create_test_dir("register_schema_integration_same_version_error");

    let content: &str = "this is my content";
    let content2: &str = "this is my content2";
    let filepath: PathBuf = root_path.join("test_model");
    let filepath2: PathBuf = root_path.join("test_model_2");
    std::fs::write(&filepath, &content).unwrap();
    std::fs::write(&filepath2, &content2).unwrap();

    let db_name: &str = "tasks";

    cmd.arg("-r");
    cmd.arg(&root_path);
    cmd.arg("register");
    cmd.arg("schema");
    cmd.arg(filepath.to_str().unwrap());
    cmd.arg(db_name);
    cmd.assert().success();

    let mut cmd = Command::cargo_bin("couchdb-orm")?;
    cmd.arg("-r");
    cmd.arg(&root_path);
    cmd.arg("register");
    cmd.arg("schema");
    cmd.arg(filepath.to_str().unwrap());
    cmd.arg("tasks");
    cmd.assert().failure().stderr(predicate::str::contains(
        r#"Error: RegisterSchemaError("schema is same as last version.\naborting.")"#,
    ));

    Ok(())
}
